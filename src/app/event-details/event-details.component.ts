import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MyaccountService} from '../services/myaccount.service';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {
  eventInfo: Array<any>;
  constructor(private route: ActivatedRoute, private eventService: MyaccountService) { }

  ngOnInit() {
    this.getEventInfo();
  }
  getEventInfo() {
    const eventId = +this.route.snapshot.paramMap.get('id');
    this.eventService.getEventById(eventId).subscribe(data => {
      this.eventInfo = data as any[];
      console.log(this.eventInfo);
    });
  }

}
