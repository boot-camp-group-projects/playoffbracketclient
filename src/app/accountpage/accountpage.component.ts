import { Component, OnInit } from '@angular/core';
import { MyaccountService} from '../services/myaccount.service';

@Component({
  selector: 'app-accountpage',
  templateUrl: './accountpage.component.html',
  styleUrls: ['./accountpage.component.css']
})
export class AccountpageComponent implements OnInit {
  customers: Array<any>;
  constructor(private customer: MyaccountService) { }

  ngOnInit() {
    this.customer.getAllCustomers().subscribe(data => {this.customers = data; });
  }

}




