import { NgModule } from '@angular/core';
import {AccountpageComponent} from './accountpage/accountpage.component';
import {AccountCreateComponent} from './account-create/account-create.component';
import {RouterModule, Routes} from '@angular/router';
import {BarTopComponent} from './bar-top/bar-top.component';
import {UserComponent} from './user/user.component';
import {PmComponent} from './pm/pm.component';
import {AdminComponent} from './admin/admin.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {HomepageComponent} from './homepage/homepage.component';
import {EventCreateComponent} from './event-create/event-create.component';
import {EventsComponent} from './events/events.component';
import {MyeventsComponent} from './myevents/myevents.component';
import {EventDetailsComponent} from './event-details/event-details.component';
import {RegisterComponent} from './register/register.component';
import {AboutComponent} from './about/about.component';


const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'user',
    component: UserComponent
  },
  {
    path: 'pm',
    component: PmComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'auth/login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: RegisterComponent
  },
  {
    path: 'events',
    component: EventsComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  //
  //
  // {path: '', redirectTo: '/home', pathMatch: 'full'},
  // {path: 'home', component: HomepageComponent},
  // {path: 'myAccount', component: AccountpageComponent},
  // {path: 'createAccount', component: AccountCreateComponent},
  // {path: 'navbar', component: BarTopComponent},
  // {path: 'events', component: EventCreateComponent}
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomepageComponent},
  {path: 'myAccount', component: AccountpageComponent},
  {path: 'createAccount', component: AccountCreateComponent},
  {path: 'home/createAccount', component: AccountCreateComponent},
  {path: 'bar-top', component: BarTopComponent},
  {path: 'events-create', component: EventCreateComponent},
  {path: 'events', component: EventsComponent},
  {path: 'myevents', component: MyeventsComponent},
  {path: 'events', component: EventsComponent},
  {path: 'events/:id', component: EventDetailsComponent},
  {path: 'about', component: AboutComponent},
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }





