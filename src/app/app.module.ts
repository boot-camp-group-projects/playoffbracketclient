import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {AgmCoreModule} from '@agm/core';
import {MapComponent} from './map/map.component';
import { BarTopComponent } from './bar-top/bar-top.component';
import { HomepageComponent } from './homepage/homepage.component';
import { EventsComponent } from './events/events.component';
import { AppRoutingModule } from './app-routing.module';
import { AccountCreateComponent } from './account-create/account-create.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EventCreateComponent } from './event-create/event-create.component';
import { AdminComponent } from './admin/admin.component';
import { PmComponent } from './pm/pm.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import {AccountpageComponent} from './accountpage/accountpage.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import {httpInterceptorProviders} from './auth/auth-interceptor';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import { MyeventsComponent } from './myevents/myevents.component';
import {
  MatButtonModule, MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule, MatNativeDateModule,
  MatRippleModule,
  MatSelectModule,
  MatStepperModule
} from '@angular/material';
import {MatFormFieldControl} from '@angular/material/typings/form-field';
import { FilterPipe } from './filter.pipe';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    BarTopComponent,
    HomepageComponent,
    EventsComponent,
    AccountpageComponent,
    AccountCreateComponent,
    EventCreateComponent,
    AdminComponent,
    PmComponent,
    RegisterComponent,
    UserComponent,
    LoginComponent,
    HomeComponent,
    MyeventsComponent,
    HomeComponent,
    EventDetailsComponent,
    FilterPipe,
    AboutComponent,
  ],
  imports: [
    BrowserModule,
    MatStepperModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA2vsKd0CJtBBzcr51sYGQeIAYuIW6XhP8'
    }),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatCheckboxModule

  ],
  providers: [
    httpInterceptorProviders,
    {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }









