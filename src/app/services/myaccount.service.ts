import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class MyaccountService {

  constructor(private http: HttpClient) { }

  public getAllCustomers(): Observable<any> {
    return this.http.get<any>('//localhost:8080/customers');
  }
  public getCustomerByEmail(email: string): Observable<any> {
    return this.http.get<any>('//localhost:8080/customers' + email);
  }
  public createCustomer(customer: object) {
    return this.http.post('//localhost:8080/customers/create', customer);
  }
  public getOrderByCustomerId(id: number): Observable<any> {
    return this.http.get<any>('//localhost:8080/orders' + id);
  }
  public getAllEvents(): Observable<any> {
    return this.http.get<any>('//localhost:8080/events');
  }
  // public getEventByCustomerId(id: number): Observable<any> {
  //   return this.http.get<any>('//localhost:8080/events/' + id);
  // }
  public getEventById(id: number): Observable<any> {
    return this.http.get('//localhost:8080/events/' + id);
  }
  public getCustomerById(id: number): Observable<any> {
    return this.http.get<any>('//localhost:8080/customers/' + id);
  }
  createEvent(obj): Observable<any> {
    return this.http.post('//localhost:8080/events/create', obj);
  }
  public getAllTypes(): Observable<any> {
    return this.http.get<any>('//localhost:8080/types');
  }

  public getUserByName(name): Observable<any> {
    return this.http.get('//localhost:8080/user/' + name);
  }

  public getEventByUserId(id): Observable<any> {
    return this.http.get('//localhost:8080/myevents/' + id);
  }

  public getEventByUsername(name): Observable<any> {
    return this.http.get('//localhost:8080/myevents/' + name);
  }

  addAttendee(obj) {
    return this.http.post('//localhost:8080/addAttendee/', obj);
  }

//   private baseUrl = 'https://party-back-brash-wolverine.apps.pcf.pcfhost.com';
//
//
// public getAllCustomers(): Observable<any> {
//     return this.http.get<any>(this.baseUrl + '/customers');
//   }
//   public getCustomerByEmail(email: string): Observable<any> {
//     return this.http.get<any>(this.baseUrl + '/customers' + email);
//   }
//   public createCustomer(customer: object) {
//     return this.http.post( this.baseUrl + '/customers/create', customer);
//   }
//   public getOrderByCustomerId(id: number): Observable<any> {
//     return this.http.get<any>( this.baseUrl + '/orders' + id);
//   }
//   public getAllEvents(): Observable<any> {
//     return this.http.get<any>(this.baseUrl + '/events');
//   }
//   public getEventByCustomerId(id: number): Observable<any> {
//     return this.http.get<any>(this.baseUrl + '/events/' + id);
//   }
//   public getCustomerById(id: number): Observable<any> {
//     return this.http.get<any>( this.baseUrl + '/customers/' + id);
//   }
//   createEvent(obj) {
//     return this.http.post(this.baseUrl + '/events/create', obj);
//   }
//   public getAllTypes(): Observable<any> {
//     return this.http.get<any>(this.baseUrl + '/types');
//   }
//
//   public getUserByName(name): Observable<any> {
//     return this.http.get( this.baseUrl + '/user/' + name);
//   }
//
//   public getEventByUsername(name): Observable<any> {
//     return this.http.get(this.baseUrl + '/myevents/' + name);
//   }


}
