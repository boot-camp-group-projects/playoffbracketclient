import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MyaccountService} from '../services/myaccount.service';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-bar-top',
  templateUrl: './bar-top.component.html',
  styleUrls: ['./bar-top.component.css']
})
export class BarTopComponent implements OnInit {
  username: string;
  password: string;
  customer: Array<any>;
  public authority: string;
  info: any;
  private roles: string[];
  loggedIn = false;

  // private password = new BehaviorSubject(<string>)()
  // password$ = this.password.asObservable();

  constructor(private router: Router, private account: MyaccountService, private tokenStorage: TokenStorageService, private token: TokenStorageService) { }



  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        } else if (role === 'ROLE_PM') {
          this.authority = 'pm';
          return false;
        }
        this.authority = 'user';
        return true;
      });
      this.info = {
        token: this.token.getToken(),
        username: this.token.getUsername(),
        authorities: this.token.getAuthorities()
      };
      this.loggedIn = true;
      console.log(this.loggedIn);
    }

// Get the modal
    const modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
    window.onclick = (event) => {
      if (event.target === modal) {
        modal.style.display = 'none';
      }
    };
  }
  logout() {
    this.token.signOut();
    window.location.assign('/home');
  }

  // login(): void {
  //   if (this.username === 'admin' && this.password === 'admin') {
  //     this.router.navigate(['myAccount']);
  //   } else {
  //     alert('Invalid credentials');
  //   }
  //
  // }
}


