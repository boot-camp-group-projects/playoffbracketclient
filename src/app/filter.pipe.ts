import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterPipe',
  pure: false
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], typeSearch: string) {
    if (items && items.length) {
      // console.log(items);
      return items.filter(item => {
        // console.log(item.type.name);
        if (typeSearch && item.type.name.toLowerCase().indexOf(typeSearch.toLowerCase()) === -1) {
          console.log(typeSearch);
          return false;
        }
      });
    } else {
      return items;
    }
  }

}
