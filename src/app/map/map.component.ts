import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  lat = 41.0602;
  lng = -111.9711;

  latBDUBS = 41.077994;
  lngBDUBS = -111.978506;


  constructor() { }

  ngOnInit() {
  }

}
