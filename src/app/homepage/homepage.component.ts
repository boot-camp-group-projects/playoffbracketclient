import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MyaccountService} from '../services/myaccount.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  event: Array<any>;

  constructor(private myaccountService: MyaccountService) { }

  ngOnInit() {
    this.getAllEvents();
  }

  getAllEvents() {
    this.myaccountService.getAllEvents().subscribe( data => {
      this.event = data;
    });
  }

}
