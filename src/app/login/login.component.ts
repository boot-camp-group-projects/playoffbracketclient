import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth/auth.service';
import { TokenStorageService } from '../auth/token-storage.service';
import { AuthLoginInfo } from '../auth/login-info';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  private loginInfo: AuthLoginInfo;

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService, private token: TokenStorageService, private router: Router) {
  }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getAuthorities();
    }
  }

  onSubmit() {
    console.log(this.form);

    this.loginInfo = new AuthLoginInfo(
      this.form.username,
      this.form.password);

    this.authService.attemptAuth(this.loginInfo).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveAuthorities(data.authorities);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getAuthorities();
        this.reloadPage();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isLoginFailed = true;
      }
    );
  }

  closeModal() {
     }

  reloadPage() {
    // window.location.assign('/#/home');
    this.router.navigate(['home']).then(() => {
      window.location.reload();
    });
  }

  logout() {
    this.token.signOut();
    window.location.reload();
  }
}


// import { Component, OnInit } from '@angular/core';
// import {Router} from '@angular/router';
// import {MyaccountService} from '../myaccount.service';
//
// @Component({
//   selector: 'app-login',
//   templateUrl: './login.component.html',
//   styleUrls: ['./login.component.css']
// })
// export class LoginComponent implements OnInit {
//   username: string;
//   password: string;
//
//   constructor(private router: Router, private account: MyaccountService) { }
//
//   ngOnInit() {
// // Get the modal
//     const modal = document.getElementById('id01');
//
// // When the user clicks anywhere outside of the modal, close it
//     window.onclick = (event) => {
//       if (event.target === modal) {
//         modal.style.display = 'none';
//       }
//     };
//   }
//
//   login(): void {
//     if (this.username === 'admin' && this.password === 'admin') {
//       this.router.navigate(['myAccount']);
//     } else {
//       alert('Invalid credentials');
//     }
//
//   }
// }
