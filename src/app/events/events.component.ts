import { Component, OnInit } from '@angular/core';
import {MyaccountService} from '../services/myaccount.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  events: Array<any>;
  types: Array<any>;
  filteredEvents: Array<any>;
  typeSearch: any;
  constructor(private myaccountService: MyaccountService) { }

  ngOnInit() {
// Get the modal
    const modal = document.getElementById('id02');

    this.myaccountService.getAllEvents().subscribe( data => {
      this.events = data as any[];
      this.filteredEvents = this.events;
      // console.log(this.filteredEvents);
    });
    this.myaccountService.getAllTypes().subscribe( data => {
      this.types = data as any[];
    });

// When the user clicks anywhere outside of the modal, close it
    window.onclick = (event) => {
      if (event.target === modal) {
        modal.style.display = 'none';
      }
    };
  }

}
