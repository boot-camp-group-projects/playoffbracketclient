import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../auth/token-storage.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  info: any;
  role: any;

  constructor(private token: TokenStorageService) { }

  ngOnInit() {
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities()
    };
    console.log(this.info);
    this.role = this.token.getAuthorities();
  }

  logout() {
    this.token.signOut();
    window.location.reload();
  }
}
