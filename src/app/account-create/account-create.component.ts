import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MyaccountService} from '../services/myaccount.service';

@Component({
  selector: 'app-account-create',
  templateUrl: './account-create.component.html',
  styleUrls: ['./account-create.component.css']
})

export class AccountCreateComponent implements OnInit {
  newCustomer: FormGroup;
  constructor(private service: MyaccountService) { }


  ngOnInit() {
    this.newCustomer = new FormGroup({
      firstname: new FormControl(),
      lastname: new FormControl(),
      address: new FormControl(),
      zipcode: new FormControl(),
      email: new FormControl(),
      phone: new FormControl(),
      username: new FormControl(),
      password: new FormControl()
    });
  }
  public createCustomer() {
    const Ob = {
      firstname: this.newCustomer.controls.firstname.value,
      lastname: this.newCustomer.controls.lastname.value,
      address: this.newCustomer.controls.address.value,
      zipcode: this.newCustomer.controls.zipcode.value,
      email: this.newCustomer.controls.email.value,
      phone: this.newCustomer.controls.phone.value,
      username: this.newCustomer.controls.username.value,
      password: this.newCustomer.controls.password.value
    };
    this.service.createCustomer(Ob).subscribe();
    this.newCustomer.reset();
  }
}
