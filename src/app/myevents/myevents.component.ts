import { Component, OnInit } from '@angular/core';
import {MyaccountService} from '../services/myaccount.service';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-myevents',
  templateUrl: './myevents.component.html',
  styleUrls: ['./myevents.component.css']
})
export class MyeventsComponent implements OnInit {
  userEvents: any[];
  info: any;

  constructor(private myaccountService: MyaccountService, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    this.info = {
      username: this.tokenStorage.getUsername()
    };
    this.getEventsByUsername();
  }

  getEventsByUsername() {
    this.myaccountService.getEventByUsername(this.info.username).subscribe( data => {
      this.userEvents = data;
    });
  }

}
