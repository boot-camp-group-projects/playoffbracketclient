import { Component, OnInit } from '@angular/core';
import {MyaccountService} from '../services/myaccount.service';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {TokenStorageService} from '../auth/token-storage.service';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-events-create',
  templateUrl: './event-create.component.html',
  styleUrls: ['./event-create.component.css']
})
export class EventCreateComponent implements OnInit {
  eventForm: FormGroup;
  attendeeForm: FormGroup
  event: Array<any>;
  types: Array<any>;
  userId: number;
  users: Array<any>;
  attendee = [];
  eventId: number;
  attendeeObject = [];


  constructor(private myaccountService: MyaccountService, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    this.getAllEvents();
    this.getAllTypes();
    this.getUser();
    this.getAllCustomers();

    this.eventForm = new FormGroup({
      name: new FormControl(''),
      type: new FormControl(''),
      starttime: new FormControl(''),
      endtime: new FormControl(''),
      address: new FormControl(''),
      city: new FormControl(''),
      state: new FormControl('')
    });

    this.attendeeForm = new FormGroup({
      user: new FormControl()
    });
  }

  getAllCustomers() {
    this.myaccountService.getAllCustomers().subscribe( data => {
      this.users = data;
      console.log(this.users);
      this.users.forEach(i => {
        if (i.id === this.userId) {
          console.log(i.id);
          console.log(this.userId);
          this.users.splice(this.users.indexOf(this.userId), 1);
          console.log(this.users);

        }
      })
      console.log(this.users);
    });
  }

  // addToArray(id) {
  //   if (this.attendee.includes(id)) {
  //     this.attendee.splice(this.attendee.indexOf(id), 1);
  //     console.log(this.attendee);
  //   } else {
  //     this.attendee.push(id);
  //     console.log(this.attendee);
  //   }
  // }

  getAllEvents() {
    this.myaccountService.getAllEvents().subscribe( data => {
      this.event = data;
    });
  }
  getAllTypes() {
    this.myaccountService.getAllTypes().subscribe( data => {
      this.types = data;
    });
  }

  getUser() {
    this.myaccountService.getUserByName(this.tokenStorage.getUsername()).subscribe( data => {
      this.userId = data;
    });
  }


  createEvent() {
    const obj = {
      name: this.eventForm.controls.name.value,
      type: this.eventForm.controls.type.value ? {id : this.eventForm.controls.type.value} : null,
      starttime: this.eventForm.controls.starttime.value,
      endtime: this.eventForm.controls.endtime.value,
      address: this.eventForm.controls.address.value,
      city: this.eventForm.controls.city.value,
      state: this.eventForm.controls.state.value,
      zipcode: 84040,
      createdBy: this.userId ? {id : this.userId} : null

    };

    // console.log(this.playerForm.controls.name.value);
    this.myaccountService.createEvent(obj).subscribe(data => {
      this.eventId = data.id;
      this.submitAttendees();
    });
    this.eventForm.reset();
  }

  addToArray(id) {
    if (this.attendee.includes(id)) {
      this.attendee.splice(this.attendee.indexOf(id), 1);
      console.log(this.attendee);
    } else {
      this.attendee.push(id);
      console.log(this.attendee);
    }
  }

  submitAttendees() {
    this.attendee.forEach(user => {
      const obj = {
        event: {id: this.eventId},
        user: {id: user}
      };
      this.attendeeObject.push(obj);
    });
    this.myaccountService.addAttendee(this.attendeeObject).subscribe();
    console.log(this.attendeeObject);
  }

}
